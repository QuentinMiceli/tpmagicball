﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MagicBall
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //Liste de réponses au format string
        List<string> answers = new List<string>(){
            "Oui !",
            "Je ne sais pas...",
            "Tu rêves !",
            "Alors là... Non",
            "Sans l'ombre d'un doute",
            "Peut être",
            "Non.",
            "La réponse est dans ton coeur :)",
            "Ok c'est plus drôle par contre",
            "Tu crois vraiment ça ?",
            "Blablablabla j'ai pas écouté",
            "Tssss... ?"
        };

        //Fonction faisant un random sur la taille de la liste answers
        public string RandomAnswer()
        {
            Random rnd = new Random();
            return answers[rnd.Next(0, answers.Count)];
        }

        //Cliquer sur le bouton va changer la valeur de la textbox avec celle du string answers choisi par RandomAnswer()
        private void Answer_Click(object sender, RoutedEventArgs e)
        {
            txtAnswer.Text = RandomAnswer();
        }
    }
}
